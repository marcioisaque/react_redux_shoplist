# Shoplist

### Description

ReactJS app to create a shoplist

### Prerequisites

* Node.js
* Yarn
* create-react-app

### Installing
```
Yarn install
```
### Running
```
Yarn start
```

### Built with

* Axios
* ESlint
* ESlint - airbnb
* ESlint - babel
* Fontawesome
* MaterialUI
* Redux
* ReactJS
* uuidv1