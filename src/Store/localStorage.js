export const loadState = () => {
    try {
        const localStorageState = localStorage.getItem('list');
        return (localStorageState === null)
        ? undefined
        : JSON.parse(localStorageState);
    } catch (err) {
        return err;
    }
};

export const saveState = (state) => {
    try {
        const localStorageState = JSON.stringify(state);
        return localStorage.setItem('list', localStorageState);
    } catch (err) {
        return err;
    }
};
