import { createStore, compose } from 'redux';
import reducers from './Ducks';
import { loadState, saveState } from './localStorage';

const localStorageState = loadState();

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const store = createStore(reducers, localStorageState, composeEnhancers());

store.subscribe(() => {
    saveState({ list: store.getState().list });
});

export default store;
