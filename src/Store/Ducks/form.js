export const Types = {
    START_UPDATE: 'form/START_UPDATE',
    FINISH_UPDATE: 'form/FINISH_UPDATE',
    START_ADD: 'form/START_ADD',
    FINISH_ADD: 'form/FINISH_ADD',
};

const INITIAL_STATE = {
    action: null,
    productToUpdate: {},
    listToUpdate: null,
};

export default function Form(state = INITIAL_STATE, action) {
    switch (action.type) {
        case Types.START_UPDATE:
            return {
                action: 'update',
                listToUpdate: action.list,
                productToUpdate: action.product,
            };
        case Types.FINISH_UPDATE:
            return {
                action: null,
                listToUpdate: null,
                productToUpdate: {},
            };
        case Types.START_ADD:
            return {
                ...state,
                action: 'new',
                listToUpdate: action.list,
            };
        case Types.FINISH_ADD:
            return {
                action: null,
                listToUpdate: null,
                productToUpdate: {},
            };
        default:
            return state;
    }
}

export const Creators = {
    startUpdate: (list, product) => ({
        type: Types.START_UPDATE,
        product,
        list,
    }),

    finishUpdate: () => ({
        type: Types.FINISH_UPDATE,
    }),

    startAdd: (list, product) => ({
        type: Types.START_ADD,
        product,
        list,
    }),

    finishAdd: () => ({
        type: Types.FINISH_ADD,
    }),
};
