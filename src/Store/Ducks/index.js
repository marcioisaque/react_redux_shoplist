import { combineReducers } from 'redux';
import Form from './form';
import List from './list';

const indexReducer = combineReducers({
    Form,
    List,
});

export default indexReducer;
