import uuidv1 from 'uuid/v1';
import {
 getDate, toogleItem, updateProduct, getItemTotal,
} from '../../Utils/helpers';

export const Types = {
    ADD_PRODUCT: 'list/ADD_PRODUCT',
    DELETE_PRODUCT: 'list/DELETE_PRODUCT',
    TOOGLE_PRODUCT: 'list/TOOGLE_PRODUCT',
    UPDATE_PRODUCT: 'list/UPDATE_PRODUCT',
    NEW_LIST: 'list/NEW_LIST',
};

const INITIAL_STATE = {
    list: null,
    items: [],
    total: null,
};

export default function list(state = INITIAL_STATE, action) {
    switch (action.type) {
        case Types.NEW_LIST:
            return {
                ...INITIAL_STATE,
                date: getDate(),
            };
        case Types.ADD_PRODUCT:
            return {
                ...state,
                list: action.listName,
                items: [
                    ...state.items,
                    {
                        ...action.product,
                        total: getItemTotal(action.product),
                        id: uuidv1(),
                        checked: false,
                    },
                ],
            };
        case Types.DELETE_PRODUCT:
            return {
                ...state,
                items: state.items.filter((item) => item.id !== action.productId),
            };
        case Types.TOOGLE_PRODUCT:
            return {
                ...state,
                items: toogleItem(state.items, action.productId),
            };
        case Types.UPDATE_PRODUCT:
            return {
                ...state,
                list: action.listName,
                items: updateProduct(state.items, action.product),
            };
        default:
           return state;
    }
}

export const Creators = {
    addProduct: (product, listName) => ({
            type: Types.ADD_PRODUCT,
            product,
            listName,
    }),

    deleteProduct: (productId) => ({
        type: Types.DELETE_PRODUCT,
        productId,
    }),

    toogleProduct: (productId) => ({
        type: Types.TOOGLE_PRODUCT,
        productId,
    }),

    updateProduct: (product, listName) => ({
        type: Types.UPDATE_PRODUCT,
        product,
        listName,
    }),

    newList: () => ({
        type: Types.NEW_LIST,
    }),
};
