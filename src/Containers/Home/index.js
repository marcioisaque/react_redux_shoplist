import React from 'react';
import { connect } from 'react-redux';
import './styles.css';
import { bindActionCreators } from 'redux';
import NewList from '../../Components/NewList';
import List from '../../Components/CardList/ShopList';
import { getListTotal, getOpenedItems, getClosedItems } from '../../Utils/selectors';
import { Creators as ListActions } from '../../Store/Ducks/list';

const home = ({ newList, list, total, openedItems, closedItems }) => (
	<>
		<NewList newList={newList} />
		{
		list.items.length > 0
		&& (
			<List
				list={list.list}
				total={total}
				date={list.date}
				openedItems={openedItems}
				closedItems={closedItems}
			/>
		)
        }
	</>

);

const mapStateToProps = (state) => ({
    list: state.List,
    total: getListTotal(state),
    openedItems: getOpenedItems(state),
    closedItems: getClosedItems(state),
});

const mapDispatchToProps = (dispatch) => bindActionCreators(ListActions, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(home);
