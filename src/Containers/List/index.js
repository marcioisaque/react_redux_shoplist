import React from 'react';
import CreateList from './Create';
import EditList from './Edition';

const List = ({ match }) => {
    if (match.params.action === 'edition') {
        return 	<EditList match={match} />;
    }

    return <CreateList match={match} />;
};

export default List;