import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Creators as ListActions } from '../../../Store/Ducks/list';
import FormProduct from '../../../Components/FormProduct';
import Items from '../../../Components/CardList/Products';
import NewProduct from '../../../Components/NewProduct';
import '../styles.css';

class EditList extends Component {
    addProduct = (product, list) => {
        const { addProduct } = this.props;
        addProduct(product, list);
    }

    render() {
		const { updateProduct, match, list, deleteProduct, toogleProduct } = this.props;
		
        return (
	<>
		<div className="page-container">
			<FormProduct
				addProduct={this.addProduct}
				updateProduct={updateProduct}
				url={match.params.action}
				/>
			<div className="list-items-container">
				<NewProduct list={list.list} />
				{ list.items.map(
				(item) => (
					<Items
						list={list.list}
						item={item}
						deleteProduct={deleteProduct}
						toogleProduct={toogleProduct}
						key={item.id}
				/>
				),
				) }

			</div>
		</div>
	</>
        );
    }
}

const mapStateToProps = (state) => ({
    list: state.List,
});

const mapDispatchToProps = (dispatch) => bindActionCreators(ListActions, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(EditList);
