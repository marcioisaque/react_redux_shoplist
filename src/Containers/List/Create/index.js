import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Creators as ListActions } from '../../../Store/Ducks/list';
import FormProduct from '../../../Components/FormProduct';
import '../styles.css';

class CreateList extends Component {
    addProduct = (product, list) => {
        const { addProduct } = this.props;
        addProduct(product, list);
    }

    render() {
        const { match } = this.props;
        return (
	<div className="page-container">
		<FormProduct
			addProduct={this.addProduct}
			url={match.params.action} 
        />
	</div>
        );
    }
}

const mapStateToProps = (state) => ({
    list: state.List,
});

const mapDispatchToProps = (dispatch) => bindActionCreators(ListActions, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(CreateList);