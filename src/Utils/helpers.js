export const getDate = () => {
    const date = new Date();
    const options = { year: 'numeric', month: '2-digit', day: '2-digit' };
    return date.toLocaleDateString('pt-BR', options);
};

export const toogleItem = (items, productId) => {
    const index = items.findIndex((item) => item.id === productId);

    return [
        ...items.slice(0, index),
        { ...items[index], checked: !items[index].checked },
        ...items.slice(index + 1),
    ];
};

export const getItemTotal = (product) => product.price * product.quantity;

export const updateProduct = (items, product) => {
    const index = items.findIndex((item) => item.id === product.id);

    return [
        ...items.slice(0, index),
        { ...product, total: getItemTotal(product) },
        ...items.slice(index + 1),
    ];
};
