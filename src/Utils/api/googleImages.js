import axios from 'axios';

export default function getImageFromGoogle(product) {
  const params = {
    key: 'AIzaSyBoeeH_EByITiQWeRwkESYZO97rHKm7SO0',
    cx: '002163612355156523725:r9rgtveawav',
    searchType: 'image',
    lr: 'lang_pt',
    num: 1,
    q: product,
  };

  return axios.get('https://www.googleapis.com/customsearch/v1', { params })
    .then((response) => response.data.items[0].link)
    .catch((err) => err);
}
