import { createSelector } from 'reselect';

export const getListTotal = createSelector(
    (state) => state.List.items,
    (items) => items.reduce((total, item) => total + item.total, 0),
);

export const getOpenedItems = createSelector(
    (state) => state.List.items,
    (items) => items.filter((item) => !item.checked).length,
);

export const getClosedItems = createSelector(
    (state) => state.List.items,
    (items) => items.filter((item) => item.checked).length,
);
