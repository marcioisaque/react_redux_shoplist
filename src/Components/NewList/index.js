import React from 'react';
import CustomCard from '../_Common/CustomCard';

const newList = (props) => (
	<>
		<div className="page-container">
			<CustomCard containerClass="new-list-container" link="/list/create">
				<div
					onClick={() => props.newList()}
					role="presentation"
				>
					<p className="title">
                        Iniciar nova lista
					</p>
				</div>
			</CustomCard>
		</div>
	</>

);

export default newList;