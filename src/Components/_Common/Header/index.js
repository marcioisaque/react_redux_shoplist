import React from 'react';
import { AppBar } from '@material-ui/core';
import { Link } from 'react-router-dom';
import Logo from '../../../Assets/Images/Logos/header-logo.png';
import './styles.css';

export default () => (
	<>
		<AppBar className='header' color='primary' position='relative'>
			<Link to='/'>
				<div className='header-brand'>
					<img src={ Logo } alt="logo" className="header-logo" />
					<p className='header-title'>Shoplist</p>
				</div>
			</Link>
		</AppBar>
	</>
);