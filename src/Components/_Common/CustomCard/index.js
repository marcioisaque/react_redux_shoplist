import React from 'react';
import PropTypes from 'prop-types';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Divider from '@material-ui/core/Divider';
import { Link } from 'react-router-dom';
import './styles.css';

const customCard = ({ containerClass, link, action, image, footer, children }) => (
	<>
		<div className={containerClass}>
			<Link to={link}>
				<Card className="card">
					<CardActionArea onClick={action} className="card-action-area">
						{image && (
						<CardMedia
							component="img"
							className="card-img"
							height="100"
							image={image}
							title="image"
						/>
                        )}
						<CardContent className="card-content">{children}</CardContent>
					</CardActionArea>
					{footer && (
					<>
						<Divider />
						<CardActions className="card-footer">{footer}</CardActions>
					</>
                    )}
				</Card>
			</Link>
		</div>
	</>
);

customCard.propTypes = {
    containerClass: PropTypes.string.isRequired,
    children: PropTypes.element.isRequired,
    footer: PropTypes.element,
    link: PropTypes.string.isRequired,
    action: PropTypes.func,
    image: PropTypes.string
};

customCard.defaultProps = {
    footer: null,
    action: null,
    image: null
};

export default customCard;
