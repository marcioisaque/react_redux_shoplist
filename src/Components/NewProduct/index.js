import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPlusCircle } from '@fortawesome/free-solid-svg-icons';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import CustomCard from '../_Common/CustomCard';
import { Creators as FormActions } from '../../Store/Ducks/form';

const NewItem = ({ list, startAdd }) => (
	<>
		<div className="list-items-container">
			<CustomCard
				link="#"
				action={() => startAdd(list)}
				containerClass="list-item"
			>
				<>
					<p className="subtitle">Novo Produto</p>
					<FontAwesomeIcon icon={faPlusCircle} color="#E4e4e4" size="8x" />
				</>
			</CustomCard>
		</div>
	</>
);

const mapDispatchToProps = (dispatch) => bindActionCreators(FormActions, dispatch);

export default connect(null, mapDispatchToProps)(NewItem);
