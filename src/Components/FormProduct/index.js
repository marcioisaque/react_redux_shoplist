import React, { Component } from 'react';
import MenuItem from '@material-ui/core/MenuItem';
import TextField from '@material-ui/core/TextField';
import { InputAdornment } from '@material-ui/core';
import Button from '@material-ui/core/Button';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import getImageFromGoogle from '../../Utils/api/googleImages';
import { Creators as FormActions } from '../../Store/Ducks/form';

const units = ['Quilo(s)', 'Litro(s)', 'Unidade(s)'];

class Form extends Component {
    constructor(props) {
        super(props);

        this.state = {
            list: '',
            product: '',
            quantity: '',
            unity: '',
            price: '',
            showErrors: false,
        };
    }

    componentDidUpdate(prevProps) {
        const { form } = this.props;


        if (form.action === 'update' && prevProps.form.productToUpdate !== form.productToUpdate) {
            const {
                product, quantity, unity, price,
            } = form.productToUpdate;

            this.alterState({
                list: form.listToUpdate,
                product,
                quantity,
                unity,
                price,
                showErrors: false,
            });
            
        } else if (form.action === 'new' && prevProps.form.productToUpdate !== form.productToUpdate) {
            this.alterState({
                list: form.listToUpdate,
                product: '',
                quantity: '',
                unity: '',
                price: '',
                showErrors: false,
            });
        }
    }

    alterState = (newState) => (
        this.setState(newState)
    )


    handleChange = (event) => {
        this.setState({ [event.target.name]: event.target.value });
    }

    handleSubmit = () => {
        const {
            list, product, quantity, unity, price,
        } = this.state;

        const { form } = this.props;

        if (!list || !product || !quantity || !unity || !price) {
            return this.setState({ showErrors: true });
        }

        return form.action === 'new' || form.action === null
         ? this.addItem(this.state)
         : this.updateItem(this.state);
    }

    addItem = ({
        list, product, quantity, unity, price,
    }) => {
        const { addProduct, finishAdd, url } = this.props;

        getImageFromGoogle(product)
        .then((res) => addProduct({
            product, quantity, unity, price, img: res,
        }, list));


        finishAdd();

        if (url === 'create') {
            window.history.go(-1);
        }
    }

    updateItem = ({
        list, product, quantity, unity, price,
    }) => {
        const { form, updateProduct, finishUpdate } = this.props;
        const { id, checked } = form.productToUpdate;

        getImageFromGoogle(product)
        .then((res) => updateProduct({
            product, quantity, unity, price, img: res, id, checked,
        }, list));

        finishUpdate();
    }

    clearState = () => {
        this.setState({
            product: '',
            quantity: '',
            unity: '',
            price: '',
            showErrors: false,
        });
    }


    render() {
        const { showForm } = this.props;
        const{ list, showErrors, product, quantity, unity, price } = this.state;


        if (!showForm) {
            return (
	            <></>
            );
        }

        return (
	<>
		<form className="form-container">
			<div className="form-row">
				<TextField
					label="Lista"
					name="list"
					value={list}
					onChange={this.handleChange}
					required
					error={!list && showErrors}
                />
				<Button variant="outlined" color="primary" onClick={this.handleSubmit}>Salvar</Button>
			</div>
			<div className="form-row">
				<TextField
					label="Produto"
					name="product"
					value={product}
					onChange={this.handleChange}
					required
					error={!product && showErrors}
                />
				<TextField
					label="Quantidade"
					name="quantity"
					value={quantity}
					onChange={this.handleChange}
					required
					error={!quantity && showErrors}
                />
				<TextField
					label="Unidade"
					select
					name="unity"
					value={unity}
					onChange={this.handleChange}
					required
					error={!unity && showErrors}
                >
					{units.map((option) => (
						<MenuItem key={option} value={option}>{option}</MenuItem>
                                ))}
				</TextField>
				<TextField
					label="Preço"
					name="price"
					value={price}
					onChange={this.handleChange}
					InputProps={{
                                startAdornment: <InputAdornment position="start">R$</InputAdornment>,
                            }}
                />
			</div>
		</form>
	</>
        );
    }
}

const mapStateToProps = (state, ownProps) => ({
    form: state.Form,
    showForm: state.Form.action || ownProps.url === 'create',
});

const mapDispatchToProps = (dispatch) => bindActionCreators(FormActions, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Form);