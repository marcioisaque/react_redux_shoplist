import React from 'react';
import PropTypes from 'prop-types';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

const Item = ({ icon, text }) => (   
	<> 
		<div className='list-card-item'>
			<p>
				<FontAwesomeIcon icon={ icon } size='sm'/>&nbsp;
				{text}
			</p>
		</div>
	</>
);

Item.propTypes = {
    icon: PropTypes.shape.isRequired,
    text: PropTypes.string.isRequired,
};

export default Item;