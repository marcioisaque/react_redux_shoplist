import React from 'react';
import { faShoppingBasket, faCheck } from '@fortawesome/free-solid-svg-icons';
import CustomCard from '../../_Common/CustomCard';
import Footer from './Footer';
import Item from './Item';

const newList = ({ total, list, openedItems, closedItems, date }) => (
	<>
		<div className='page-container'>
			<CustomCard 
				containerClass='list-container' 
				footer={ <Footer total={total} date={date}/> } 
				link='/list/edition'
            >
				<>
					<p className='title'>
						{list}
					</p>
					<div className='list-card-body'>
						<Item icon={ faShoppingBasket } text={`${openedItems} item(s) restante(s)`} />
						<Item icon={ faCheck } text={`${closedItems} item(s) comprado(s)`}  />
					</div>
				</>
			</CustomCard>
		</div>
	</>

);

export default newList;