import React from 'react';

const Footer = ({ total, date }) => (
	<>
		<div className="list-footer">
			<small>Criada em: { date }</small>
			<p>R$ {total}</p>
		</div>
	</>
);

export default Footer;