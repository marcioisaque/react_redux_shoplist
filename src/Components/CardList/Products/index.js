import React from 'react';
import Checkbox from '@material-ui/core/Checkbox';
import Typography from '@material-ui/core/Typography';
import CustomCard from '../../_Common/CustomCard';
import Footer from './Footer';

const Products = ({item, deleteProduct, toogleProduct, list}) => (   

	<>
		<div className='list-items-container'>
			<CustomCard 
				link='#' 
				containerClass='list-item'
				image={item.img}
				action={() => toogleProduct(item.id)}
				footer={<Footer item={item} list={list} deleteProduct={deleteProduct}/>}
                    >
				<>
					<div className='list-item-header'>
						<Typography variant='subtitle1' component='h2'>{item.product}</Typography>
						<Checkbox checked={item.checked}/>
					</div>
					<Typography component='p'>{ `${item.quantity} ${item.unity}` }</Typography>
					<Typography component='p'>R$ {item.price}</Typography>
				</>
			</CustomCard>
		</div>
	</>
);

export default Products;