import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import Header from './Components/_Common/Header';
import HomePage from './Containers/Home';
import List from './Containers/List';
import './index.css';
import store from './Store';

const theme = createMuiTheme({
  typography: {
    useNextVariants: true,
  },
  palette: {
    primary: {
      main: '#ff5722',
    },
    secondary: {
      main: '#ffc400',
    },
  },
});

ReactDOM.render(<Provider store={store}>
	<MuiThemeProvider theme={theme}>
		<Router>
			<Switch>
				<>
					<Header />
					<Route exact path="/" component={HomePage} />
					<Route exact path="/list/:action" component={List} />
				</>
			</Switch>
		</Router>
	</MuiThemeProvider>
</Provider>, document.getElementById('root'));
